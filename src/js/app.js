/*globals require*/

'use strict';
window.ko = require('ko');
var IconView = require('./widgets/IconView/IconView');

var testWidget = new IconView({
  container: 'icon-view-box',
  containerWidth: 300,
  containerHeight: 300,
  itemWidth: 64,
  itemHeight: 70,

  items: [{
    id: 'item_1',
    text: 'text_item_1',
    img: 'images/ico1_32x32.png',
    checked: true
  },{
    id: 'item_2',
    text: 'text_item_2',
    img: 'images/ico2_32x32.png',
    checked: true
  },{
    id: 'item_3',
    text: 'text_item_3',
    img: 'images/ico3_32x32.png',
    checked: true
  },{
    id: 'item_4',
    text: 'text_item_4',
    img: 'images/ico4_32x32.png',
    checked: false
  },{
    id: 'item_5',
    text: 'text_item_5',
    img: 'images/ico5_32x32.png',
    checked: false
  }],

  events: {
    addedItems: function(items){
      console.log('added ' + items.length + ' items');
    },
    removedItems: function(items){
      console.log('removed ' + items.length + ' items');
    },
    clickItem: function(item){
      console.log('clicked ' + item.id);
    }
  }
});

var removeBtn = document.querySelector('.btn-wrapper .btn-remove');
var selectBtn = document.querySelector('.btn-wrapper .btn-switch');

removeBtn.addEventListener('click', function(){
  testWidget.removeChecked();
});
selectBtn.addEventListener('click', function(){
  testWidget.switchChecked();
});

setTimeout(function(){
  testWidget.addItems([{
    id: 'item_6',
    text: 'text_item_6',
    img: 'images/ico2_32x32.png',
    checked: false
  },{
    id: 'item_7',
    text: 'text_item_7',
    img: 'images/ico4_32x32.png',
    checked: false
  }])
}, 3000);

setTimeout(function(){
  testWidget.removeItems(['item_1', 'item_2']);
}, 5000);