var _ = require('underscore'),
  fs = require('fs'),
  template = fs.readFileSync(__dirname + '/template.html', 'utf8');

/**
 * Widget ItemView
 * @module widgets
 * @submodule ItemView
 * @class choister.choister-front.ItemView
 */

/**
 * You can use iconViewObj.ini(props) or initiate by constructor<br>
 *
 * ```
 * props = {
 *  container: {Object|String} DOM node or node selector
 *  containerWidth: {Number} in pixels
 *  containerHeight: {Number} in pixels
 *  itemWidth: {Number} outer size - in pixels
    itemHeight: {Number} outer size - in pixels
    items: {Array} items collection [
      {
        id: {String}
        text: {String}
        img: {String}
        checked: {Boolean}
      }
    ]

    evens: {Object} available names **clickItem** handler will colling whit props **item** {Object}, <br>
                    **addedItems, removedItems**, handler will colling whit props **items** {Array} <br>
 * }
 * ```
 * Widget api:<br>
 *   iconViewObj.ini(props) properties subscribed above <br>
 *   iconViewObj.addItems(items) it needs collection <br>
 *   iconViewObj.removeItems(itemIds) it needs array of item ids <br>
 *   iconViewObj.checkItems(itemIds|'all') it needs array of item ids or string 'all' <br>
 *   iconViewObj.uncheckItems(itemIds|'all') it needs array of item ids or string 'all' <br>
 *   iconViewObj.removeChecked(props) <br>
 *   iconViewObj.switchChecked(props) <br>
 *
 * @param props {Object}
 * @constructor
 */
function IconView(props){
  this.ViewModel = {};

  if (props) {
    this.init(props)
  }
}

/**
 * Initiate widget
 * @param props {Object|undefined}
 * @method init
 */
IconView.prototype.init = function(props){
  this.box = typeof props.container === 'string' ? document.querySelector('#' + props.container) : props.container;

  initEvents.call(this, props.events || {});
  initViewModel.call(this, props);

  if (!ko.dataFor(this.box)){
    this.box.innerHTML = template;
    ko.applyBindings(this.ViewModel, this.box);
  }

};

function initEvents(events){
  this.eventHandlers = {};

  if (events){
    this.eventHandlers.clickItem = typeof events.clickItem === 'function' ? events.clickItem : _.noop;
    this.eventHandlers.addedItems = typeof events.addedItems === 'function' ? events.addedItems : _.noop;
    this.eventHandlers.removedItems = typeof events.removedItems === 'function' ? events.removedItems : _.noop;
  }
}

function initViewModel(props){
  var vm = this.ViewModel;

  vm.containerWidth = props.containerWidth + 'px';
  vm.containerHeight = props.containerHeight + 'px';
  vm.itemWidth = props.itemWidth + 'px';
  vm.itemHeight = props.itemHeight + 'px';

  var items = props.items && props.items.length ? props.items : [];

  // we need be able to watch the "checked" field
  if (items.length){
    items.forEach(function(item){
      if (!item.chekedValue) {
        item.checkedValue = ko.observable(!!item.checked);
      }
    });
  }
  vm.items = ko.observableArray(items);

  vm.itemClick = function(item){
    item.checked ? this.uncheckItems([item.id]) : this.checkItems([item.id]);
    this.eventHandlers.clickItem(item);
  }.bind(this);
}

/**
 * Add new items into end of list
 * @param items {Array} item collections
 * @method addItems
 */
IconView.prototype.addItems = function(items){
  items.forEach(function(item){
    item.checkedValue = ko.observable(!!item.checked);
  });
  // in this case KO will render only new items
  this.ViewModel.items.push.apply(this.ViewModel.items, items);
  this.eventHandlers.addedItems(items)
};

/**
 * Remove items
 * @param itemIds {Array} list of item ids
 * @method removeItems
 */
IconView.prototype.removeItems = function(itemIds){
  var vm = this.ViewModel;
  var newItems = vm.items().filter(function(item){
    return !~itemIds.indexOf(item.id)
  });
  vm.items(newItems);
  this.eventHandlers.removedItems(itemIds);
};

/**
 * Remove checked items
 * @method removeChecked
 */
IconView.prototype.removeChecked = function(){
  var vm = this.ViewModel,
    removed = [];

  var newItems = vm.items().filter(function(item){
    if (item.checked) {
      removed.push(item.id);
    }
    return !item.checked
  });
  vm.items(newItems);
  this.eventHandlers.removedItems(removed);
};

/**
 * Check list items or all
 * @param itemIds {Array|String} list of item ids or string 'all'
 * @method checkItems
 */

// TODO: merge the implementation of checkItems and uncheckItems into switch function
IconView.prototype.checkItems = function(itemIds){
  var vm = this.ViewModel,
    allFlag = !!(typeof itemIds === 'string' && itemIds === 'all');

  vm.items().forEach(function(item){
    if (!item.checked && ( allFlag || ~itemIds.indexOf(item.id)) ){
      item.checked = true;
      item.checkedValue(true);
    }
  });
};

/**
 * Uncheck list of items or all
 * @param itemIds itemIds {Array|String} list of item ids or string 'all'
 * @method uncheckItems
 */
IconView.prototype.uncheckItems = function(itemIds){
  var vm = this.ViewModel,
    allFlag = !!(typeof itemIds === 'string' && itemIds === 'all');

  vm.items().forEach(function(item){
    if (item.checked && ( allFlag || ~itemIds.indexOf(item.id)) ){
      item.checked = false;
      item.checkedValue(false);
    }
  });
};

/**
 * Uncheck checked items or check if there is't checked items
 * @method switchChecked
 */
IconView.prototype.switchChecked = function(){
  var vm = this.ViewModel,
    hasChecked = vm.items().some(function(item){
      return item.checked;
    });
  hasChecked ? this.uncheckItems('all') : this.checkItems('all');
};


module.exports = IconView;