module.exports = function (grunt) {
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        // Project settings
        config: {
            // Configurable paths
            app: 'src',
            build: 'build'
        },
        // Watches files for changes and runs tasks based on the changed files
        watch: {
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= config.app %>/*.html',
                    '<%= config.app %>/js/**/*.*',
                    '<%= config.app %>/scss/**/*.*',
                    '<%= config.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    '<%= config.app %>/{,*/}*.html',
                    '<%= config.app %>/images/{,*/}*'
                ],
                tasks: ['build']
            }
        },
        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                livereload: 35729,
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    protocol: 'http',
                    open: 'http://localhost:9000/',
                    base: [
                        '.tmp',
                        '<%= config.build %>'
                    ]
                }
            }
        },
        clean: {
            tmp: {
                src: ['tmp/*']
            },
            all: {
              src: ['tmp/*', 'build/*']
            }
        },
        sass: {
            dist: {
                options: {
                  sourcemap: 'none'
                },

                files: [{
                    'src/css/main.css': 'src/scss/main.scss'
                  },{
                    expand: true,
                    cwd: 'src/js/widgets',
                    src: ['**/*.scss'],
                    dest: 'tmp/css/widgets',
                    ext: '.css'
                }]
            }
        },

        browserify: {
            options: {
                alias: {
                    'ko': './src/bower_components/knockout/dist/knockout',
                    'underscore': './src/bower_components/underscore/underscore'
                },
                transform: ['brfs-htmlmin'] // , 'uglifyify'
            },
            build: {
                src: './src/js/app.js',
                dest: './build/js/app.js'
            }
        },

        copy: {
            build: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.build %>',
                    src: [
                        'images/**/*.*',
                        'css/**/*.*',
                        'index.html',
                        'fonts/*.*',
                        'tmp/css/**/*.*'
                    ]
                },{
                  expand: true,
                  flatten: true,
                  cwd:'tmp/css/widgets',
                  src: ['**/*.css'],
                  dest: '<%= config.build %>/css'
                }]
            }
        }
    });

    grunt.registerTask('server', [
      'build',
      'connect:livereload',
      'watch'
    ]);


    grunt.registerTask('build', [
        'clean:all',
        'browserify',
        'sass',
        'copy:build'
    ]);

    grunt.registerTask('default', ['build']);
};